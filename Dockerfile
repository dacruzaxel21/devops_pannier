FROM maven:3.8.3-jdk-11 as builder
WORKDIR /maven/pannier
COPY . ./
RUN mvn package

FROM openjdk:11-slim
RUN useradd -ms /bin/bash spring
USER spring
WORKDIR /home/spring
COPY --from=builder /maven/pannier/target/*.war app.war
ENTRYPOINT ["java","-jar","app.war"]
